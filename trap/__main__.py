import threading

from . import Signal, Trap

print_lock = threading.Lock()


def thread_print(msg, *args):
    with print_lock:
        print(('%-20s %s' % (threading.current_thread().name, msg)) % args)


def heartbeat(stop_event: threading.Event, max_beats: int = 10, interval: int = 1):
    thread_print("Starting heartbeat, will stop after %d beats or upon SIGINT/SIGTERM", max_beats)
    beat = 0
    while True:

        if stop_event.wait(interval):
            thread_print("Stopping because stop_event is set!")
            return

        beat += 1
        thread_print("Beat %d", beat)

        if beat >= max_beats:
            thread_print("Stopping after %d heartbeats!", beat)
            return


def main():
    stop_event = threading.Event()
    num_heartbeats = 10
    heartbeat_interval = 1

    with Trap():

        thread = threading.Thread(target=heartbeat, args=(stop_event, num_heartbeats, heartbeat_interval))
        thread.start()

        try:
            thread_print('Waiting to join heartbeat thread...')
            thread.join()
            thread_print('Thread terminated (join() returned)')
        except Signal as e:
            thread_print('%s caught!', e)
            stop_event.set()
            thread_print('Stop event was set.')
        finally:
            if thread.is_alive():
                thread_print('Thread still alive, waiting to join...')
                thread.join()
                thread_print('Thread terminated (join() returned)')
            else:
                thread_print('Thread no longer alive, no need to join.')


if __name__ == "__main__":
    main()
