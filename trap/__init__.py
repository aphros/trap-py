"""
Package trap facilitates signal handling.
"""

import signal
import threading


class Signal(BaseException):
    """
    Raised when a signal is trapped.
    """

    def __init__(self, signum, frame):
        self.signal = signal.Signals(signum)
        self.frame = frame

        super().__init__(self.signal.name)


class Trap(object):
    """
    Context manager for raising Signal exception when signals are handled.
    """

    SIGNAL_NUMBERS = [signal.SIGINT, signal.SIGTERM]
    """
    List of signal numbers trapped by default.
    """

    def __init__(self, signal_numbers=None):
        """
        Traps the given signals.
        """
        self._handled = self.SIGNAL_NUMBERS \
            if signal_numbers is None \
            else signal_numbers
        self._restore = {}

        self._raised = False

    def __enter__(self):
        """
        Installs Signal raising handler for each handled signal.
        """

        if threading.current_thread() != threading.main_thread():
            raise RuntimeError('Can only trap signals on the main thread!')

        for signum in self._handled:
            self._restore[signum] = signal.signal(signum, self._handler)
        return None

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Restores original signal handlers.
        """
        for signum, handler in self._restore.items():
            signal.signal(signum, handler)

        return False

    def _handler(self, signum, frame):
        """
        Raises signal if it was not previously raised
        """
        if not self._raised:
            raise Signal(signum, frame)
