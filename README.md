---
title: Trap - Signal Handling Context for Python
---

This package is automatically built using GitLab CI/CD facilities and
uploaded to the project-level package registry.

Note that packages uploaded to the project-level package registry are
also available via the group-level package registry.
