import setuptools

setuptools.setup(
    name="trap",
    version="1.0.0",
    author="Christoph Martin",
    author_email="1714875-c14n@users.noreply.gitlab.com",
    description="A context manager for signal handling.",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
